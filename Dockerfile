FROM java:8-jdk-alpine
COPY wsprovider-0.0.1.jar /usr/app/
WORKDIR /usr/app
EXPOSE 9007
ENTRYPOINT ["java", "-jar", "wsprovider-0.0.1.jar"]
#ENTRYPOINT ["java", "-cp", "wsprovider-0.0.1.jar","com.cbs.cprint.Application"]