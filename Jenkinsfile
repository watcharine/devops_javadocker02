pipeline {
    parameters { 
        choice( name: 'DEPLOY_ENV', choices: ['DEV', 'SIT' , 'UAT'], description: 'Deploy to environment?')
    }

    agent any

    tools { 
        maven 'maven363' 
        jdk 'jdk14' 
    }

        /*dockerfile {
            filename 'Dockerfile'
            dir 'docker-environment'
            args '-v $PWD:/var/java-project \
                -v /var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation:/var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation \
                -v /var/run/docker.sock:/var/run/docker.sock'
        }*/
    
    
    stages {

        stage('Initialize') {
            steps {
                script {
                    def props = readProperties file: '.env'
                    //props.each{ k,v -> println "$k = $v" }
                    props.each{ k,v -> env."${k}" = "${v}" }
					MESSAGE_TEMPLATE = "$JOB_NAME Build #$BUILD_ID " + env['MESSAGE_TEMPLATE'] + "\r\n ${BUILD_URL}input"
					DEV_HOST = "${DEV_HOST}" + "." + "${DEV_ZONE}"
					SIT_HOST = "${SIT_HOST}" + "." + "${SIT_ZONE}"
					
                    IMAGE_VERSION = "${PROJECT_VERSION}"
                    
                    echo "${IMAGE_VERSION}"
                    
                    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'jfrog-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                        sh "jfrog rt config --url=${JFROG_ARTIFACTORY_URL} --user=$USERNAME --password=$PASSWORD"
                    }
                    
                    /*dir("./docker-environment"){
                        def settings = readFile('settings.xml')
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: JFROG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                            settings = settings.replace("{JFrogUsername}", "$USERNAME")
                            settings = settings.replace("{JFrogPassword}", "$PASSWORD")
                            settings = settings.replace("{JFrogVirtualURL}", "${JFROG_ARTIFACTORY_URL}/${JFROG_VIRTUAL_REPO}")
                        }
                        writeFile file:"settings.xml", text:settings
                        sh 'cat settings.xml'
                        sh 'mv settings.xml /usr/share/maven/conf/'
                    }*/
                    
                    // Child Branch Decision
                    env.BRANCH_ENV = 1 // Default Environment : DEV
                    env.DO_SIT = false
                    
                    // Developer branch
                    if(params.DEPLOY_ENV == "DEV"){ 
                        env.BRANCH_ENV = 1 // DEV 
                    }
                    else if(params.DEPLOY_ENV == "SIT"){ 
                        env.BRANCH_ENV = 2 // DEV SIT 
                        env.DO_SIT = true
                    }
                }
            }
        }


        // stage('Maven Test') {
        //     steps {
        //         sh 'mvn test'
        //     }
        //     post {
        //         always {
        //             junit 'target/surefire-reports/*.xml'
        //         }
        //     }
        // }

        stage('Maven Deploy') {
            steps {
                script{
                    branchName = env.GIT_BRANCH.replace("origin/","") 
                    echo "${branchName}"
                    def pom = readMavenPom file: 'pom.xml'
                    ORIGINAL_ARTIFACT_ID = pom.artifactId
                    ORIGINAL_ARTIFACT_VERSION = pom.version
                    pom.version = pom.version.toString() + "-$BUILD_ID"
                    // pom.distributionManagement.repository.url = '' // clear default value
                    // pom.distributionManagement.repository.url = pom.distributionManagement.repository.url.toString + "${JFROG_ARTIFACTORY_URL}/${JFROG_LOCAL_REPO}"
                    // ****** Change project name when branch not master ******
                    if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}") {
                        pom.artifactId = pom.artifactId.toString() + "-${branchName}"
                    }
                    
                    writeMavenPom model: pom
                    sh 'cat pom.xml'   
		    //sh 'pwd'		 
                        // sh "mvn versions:set -DnewVersion=${version}-$BUILD_ID -DnewArtifactId=${artifactId}-${branchName}" //
                    sh "mvn install:install-file -Dfile=./lib/ScJDBC-3.1.7.jar -DgroupId=sc.jdbc -DartifactId=scjdbc -Dversion=3.1.7.0 -Dpackaging=jar"     
                    sh "mvn deploy"
                }
            }
        }
        
        stage('Build Docker Image & Push to Harbor Registry') {
		        steps{
                    script{
                        def pom = readMavenPom file: 'pom.xml'
                        groupId = pom.groupId
                        artifactId = pom.artifactId
                        version = pom.version
                        groupIdPath = groupId.replace(".","/")
                        artifactPath = "${groupIdPath}/${artifactId}/${version}"
                
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: JFROG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                        sh "jfrog rt dl ${JFROG_LOCAL_REPO}/${artifactPath}/${artifactId}-${version}.jar ${ORIGINAL_ARTIFACT_ID}-${ORIGINAL_ARTIFACT_VERSION}.jar --url=${JFROG_ARTIFACTORY_URL} --user=$USERNAME --password=$PASSWORD"}
                        
                        sh "mv ${artifactPath}/${ORIGINAL_ARTIFACT_ID}-${ORIGINAL_ARTIFACT_VERSION}.jar ."
                        sh 'ls'
                        
                        docker.withRegistry(DOCKER_REGISTRY_PROTOCAL + DOCKER_REGISTRY_URL, DOCKER_REGISTRY_CREDENTIALS) { 
                            docker.build("${DOCKER_REGISTRY_BASE}/${artifactId}:${version}").push()
                        }
                                          
                        sh "docker rmi ${DOCKER_REGISTRY_BASE}/${artifactId}:${version}"
                        sh "docker rmi ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${artifactId}:${version}"
                    }
                }
	    }
	    

        stage('Deploy to Kubernetes (DEV)') {
            steps{
                script{
                    if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 1){
                        // Child Branch
                        initialKubeFile("${KUBE_DEV_FILE}","${KUBE_DEV_BRANCH_NAMESPACE}","${KUBE_DEV_BRANCH_SECRET}","${DEV_BRANCH_HOST}")
                        deployToKubernetes("${KUBE_DEV_FILE}","${KUBECONFIG_DEV}")
                        dnsUpdate("${KUBE_DEV_BRANCH_HOST}","${KUBE_DEV_BRANCH_ZONE}","${KUBE_IP}")
                    }else{
                        // Master
                        initialKubeFile("${KUBE_DEV_FILE}","${KUBE_DEV_NAMESPACE}","${KUBE_DEV_SECRET}","${DEV_HOST}")
                        deployToKubernetes("${KUBE_DEV_FILE}","${KUBECONFIG_DEV}")  
                        dnsUpdate("${KUBE_DEV_HOST}","${KUBE_DEV_ZONE}","${KUBE_IP}")
                    }
                }
            }
        }

        stage('Robot Selenium Test (DEV)') {
            steps {
                script{
                    if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 1){
                        // Child Branch
                        branchName = env.GIT_BRANCH.replace("origin/","")
                        initialRobotFile("${ROBOT_DEV_BRANCH_FILE}","${DEV_BRANCH_HOST}")
						robotTest("${ROBOT_DEV_BRANCH_FILE}","DEV-${branchName}") // unknown branch

                    }else{
                        // Master
                        initialRobotFile("${ROBOT_DEV_FILE}","${DEV_HOST}")
						robotTest("${ROBOT_DEV_FILE}",'DEV') 
                    }
                }
            }
        }

        stage('JMeter Test (DEV)') {
            steps {
                script{
                    if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 1){
                        // Child Branch
                        branchName = env.GIT_BRANCH.replace("origin/","")
						initialJMeterFile("${JMETER_DEV_BRANCH_FILE}","${DEV_BRANCH_HOST}")
						jmeterTest("${JMETER_DEV_BRANCH_FILE}","DEV-${branchName}") // unknown branch
                    }else{
                        // Master
					 	initialJMeterFile("${JMETER_DEV_FILE}","${DEV_HOST}")
						jmeterTest("${JMETER_DEV_FILE}",'DEV') // master branch
                    }
                }
            }
        }
		
		stage('Code Quality Analysis') {
            steps {
                withSonarQubeEnv('sonarQlocal') {
                 sh 'mvn clean verify sonar:sonar -DskipTests'
                }
            }
		}
		
		stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                success{
                    echo "Sonar success"
                    script{
                        rocket("developer","${BUILD_URL} SonarQube : Build Succeeded")
			            rocket("it_admin","${BUILD_URL} SonarQube : Build Succeeded")
                    }
                }
                failure{
                    echo "Sonar failed"
                    script{
                        rocket("developer","${BUILD_URL} SonarQube : Build Failed")
			            rocket("it_admin","${BUILD_URL} SonarQube : Build Failed")
                    }
                }
            }
        }

        stage('Deploy to Kubernetes (SIT)') {
             when {  expression { return env.DO_SIT.toBoolean() == true; } }
             steps{
				script{
				
					if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 2){
					    initialKubeFile("${KUBE_SIT_FILE}","${KUBE_SIT_BRANCH_NAMESPACE}","${KUBE_SIT_BRANCH_SECRET}","${SIT_BRANCH_HOST}")
						deployToKubernetes("${KUBE_SIT_FILE}","${KUBECONFIG_SIT}")
						dnsUpdate("${KUBE_SIT_BRANCH_HOST}","${KUBE_SIT_BRANCH_ZONE}","${KUBE_IP}")
					}
					else{
						initialKubeFile("${KUBE_SIT_FILE}","${KUBE_SIT_NAMESPACE}","${KUBE_SIT_SECRET}","${SIT_HOST}")
						deployToKubernetes("${KUBE_SIT_FILE}","${KUBECONFIG_SIT}")
						dnsUpdate("${KUBE_SIT_HOST}","${KUBE_SIT_ZONE}","${KUBE_IP}")
					}
                }
             }
         }


        stage('Robot Selenium Test (SIT)') {
             when {  expression { return env.DO_SIT.toBoolean() == true; } }
             steps {
				script{
					if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 2){
						initialRobotFile("${ROBOT_SIT_FILE}","${SIT_HOST}")
			            robotTest("${ROBOT_SIT_FILE}",'SIT')
					}
					else{
						initialRobotFile("${ROBOT_SIT_FILE}","${SIT_HOST}")
						robotTest("${ROBOT_SIT_FILE}",'SIT') // master branch
					}
				}
            }
        }
        
        stage('JMeter Test (SIT)') {
             when {  expression { return env.DO_SIT.toBoolean() == true; } }
             steps {
				script{
					if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 2){
						initialJMeterFile("${JMETER_SIT_FILE}","${SIT_HOST}")
			            jmeterTest("${JMETER_SIT_FILE}",'SIT') 
					}
					else{
						initialJMeterFile("${JMETER_SIT_FILE}","${SIT_HOST}")
						jmeterTest("${JMETER_SIT_FILE}",'SIT') // master branch
					}
				}
            }
        }

        //stage('Aprrove for UAT') {
        //    when { 
        //        allOf {
        //            expression { return env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" }
        //            expression { return env.DO_UAT.toBoolean() == true; }
        //        } 
        //    }  
        //    parallel {
        //        stage('First Approve') {
        //            steps {
        //                echo "${MESSAGE_TEMPLATE}"
        //                notifyEmail('Approve for UAT 1!',"${MESSAGE_TEMPLATE}","${APPROVE_1}")
        //                timeout(time: 3, unit: "DAYS"){
        //                    input id: 'Approve1', message: "Deploy to UAT?, Approval by ${APPROVE_1}", ok: "Approve" ,submitter: "${APPROVE_1}"
        //                }
        //            }
        //        }

        //        stage('Second Approve') {
        //            steps {
        //                notifyEmail('Approve for UAT 2!',"${MESSAGE_TEMPLATE}","${APPROVE_2}")
        //                timeout(time: 3, unit: "DAYS"){
        //                    input id: 'Approve2', message: "Deploy to UAT?, Approval by ${APPROVE_2}", ok: "Approve" ,submitter: "${APPROVE_2}"
        //                }
        //            }
        //        }
        //    }
        //}

        stage('Deploy to Kubernetes (UAT)') {
            when {  expression { return env.DO_UAT.toBoolean() == true; } }
            steps{
				script{
					if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 3){
						initialKubeFile("${KUBE_UAT_FILE}","${KUBE_UAT_BRANCH_NAMESPACE}","${KUBE_UAT_BRANCH_SECRET}","${UAT_BRANCH_HOST}")
						deployToKubernetes("${KUBE_UAT_FILE}","${KUBECONFIG_UAT}")
						dnsUpdate("${KUBE_UAT_BRANCH_HOST}","${KUBE_UAT_BRANCH_ZONE}","${KUBE_IP}")
					}
					else{
						initialKubeFile("${KUBE_UAT_FILE}","${KUBE_UAT_NAMESPACE}","${KUBE_UAT_SECRET}","${UAT_HOST}")
						deployToKubernetes("${KUBE_UAT_FILE}","${KUBECONFIG_UAT}")
						dnsUpdate("${KUBE_UAT_HOST}","${KUBE_UAT_ZONE}","${KUBE_IP}")
					}
				}
            }
        }

        stage('Robot Selenium Test (UAT)') {
            when {  expression { return env.DO_UAT.toBoolean() == true; } }
            steps {
				script{
					if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 3){
						initialRobotFile("${ROBOT_UAT_FILE}","${UAT_HOST}")
				        robotTest("${ROBOT_UAT_FILE}",'UAT')  // branch
					}
					else{
						initialRobotFile("${ROBOT_UAT_FILE}","${UAT_HOST}")
						robotTest("${ROBOT_UAT_FILE}",'UAT')  // master branch
					}
				}
            }
        }

        stage('JMeter Test (UAT)') {
            when {  expression { return env.DO_UAT.toBoolean() == true; } }
            steps {
				script{
					if(env.GIT_BRANCH != "${BRANCH_MASTER_NAME}" && BRANCH_ENV.toInteger() >= 3){
						initialJMeterFile("${JMETER_UAT_FILE}","${UAT_HOST}")
				        jmeterTest("${JMETER_UAT_FILE}",'UAT') // master branch
					}
					else{
						initialJMeterFile("${JMETER_UAT_FILE}","${UAT_HOST}")
						jmeterTest("${JMETER_UAT_FILE}",'UAT') // master branch
					}
				}
            }
        }
        
        stage('Aprrove for production') {
            when {  expression { return env.DO_PROD.toBoolean() == true; } }
            parallel {
                stage('First Approve') {
                    steps {
                        echo "${MESSAGE_TEMPLATE}"
                        notifyEmail('Approve for production 1!',"${MESSAGE_TEMPLATE}","${APPROVE_1}")
                        timeout(time: 3, unit: "DAYS"){
                            input id: 'Approve1', message: "Deploy to production?, Approval by ${APPROVE_1}", ok: "Approve" ,submitter: "${APPROVE_1}"
                        }
                    }
                }

                stage('Second Approve') {
                    steps {
                        notifyEmail('Approve for production 2!',"${MESSAGE_TEMPLATE}","${APPROVE_2}")
                        timeout(time: 3, unit: "DAYS"){
                            input id: 'Approve2', message: "Deploy to production?, Approval by ${APPROVE_2}", ok: "Approve" ,submitter: "${APPROVE_2}"
                        }
                    }
                }
            }
        }

        stage('Deploy to Kubernetes (PROD)') {
            when {  expression { return env.DO_PROD.toBoolean() == true; } }
            steps{
                initialKubeFile("${KUBE_PROD_FILE}","${KUBE_PROD_NAMESPACE}","${KUBE_PROD_SECRET}","${PROD_HOST}")
				deployToKubernetes("${KUBE_PROD_FILE}","${KUBECONFIG_PROD}")
				dnsUpdate("${KUBE_PROD_HOST}","${KUBE_PROD_ZONE}","${KUBE_IP}")
            }
        }

    }
    post { 
        always {
            // Publish Robot Report
            script {
                step(
                    [
                        $class              : 'RobotPublisher',
                        outputPath          : 'robot-result',
                        outputFileName      : "**/output.xml",
                        reportFileName      : '**/report.html',
                        logFileName         : '**/log.html',
                        disableArchiveOutput: false,
                        passThreshold       : 100,
                        unstableThreshold   : 95,
                        otherFiles          : "**/*.png,**/*.jpg",
                    ]
                )
            }
            deleteDir() /* clean up our workspace */
        }
        success{
            echo "Post all stage success"
            script{
                rocket("developer","${BUILD_URL} Pipeline : Build Succeeded")
		        rocket("it_admin","${BUILD_URL} Pipeline : Build Succeeded")
            }
        }
        failure{
            echo "Post all stage failed"
            script{
                rocket("developer","${BUILD_URL} Pipeline : Build Failed")
		        rocket("it_admin","${BUILD_URL} Pipeline : Build Failed")
            }
        }
    }
}

def initialKubeFile(fileName,namespace,secret,host){
	script{
		dir("./kubernetes-deployment"){
            sh "ls -lt"
            sh "cat ${fileName}"
			image = "${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${artifactId}:${version}"
			def kube = readFile("${fileName}")
			kube = kube.replace("{KUBE_APP_NAME}", "${KUBE_APP_NAME}");
            kube = kube.replace("{KUBE_CONTAINER_PORT}", "${KUBE_CONTAINER_PORT}");
            kube = kube.replace("{KUBE_SERVICE_PORT}", "${KUBE_SERVICE_PORT}");

			kube = kube.replace("{KUBE_NAMESPACE}", "${namespace}");
			kube = kube.replace("{KUBE_IMAGE}", "${image}")
			kube = kube.replace("{KUBE_SECRET}", "${secret}");
			kube = kube.replace("{KUBE_INGRESS_HOST}", "${host}");
			writeFile file:"${fileName}", text:kube
            sh "cat ${fileName}"
		}
	}
}

def initialJMeterFile(fileName,host){
	script{
		dir("./jmeter-test"){
			def jmeter = readFile("${fileName}")
			jmeter = jmeter.replace("{JMETER_HOST}", "${host}")
			writeFile file:"${fileName}", text:jmeter
		}
	}
}

def initialRobotFile(fileName,host){
	script{
		dir("./robot-test"){
			def robot = readFile("${fileName}")
			robot = robot.replace("{ROBOT_HOST}", "http://" + "${host}")
			writeFile file:"${fileName}", text:robot
            sh "cat ${fileName}"
		}
	}
}

def rocket(channel,message){
    rocketSend channel: "${channel}", message: "${message}", rawMessage: true, serverUrl: "${ROCKET_URL}", trustSSL: true, webhookToken: "${ROCKET_TOKEN}"
}

def notifyEmail(subject,msg,to) {
    emailext body: "${msg}", subject: "${subject}", to: "${to}"
}

def deployToKubernetes(deployment,credential){
    echo "${credential}"
    script{
        withCredentials([file(credentialsId: "${credential}", variable: 'config')]) {   
            sh "mv \$config ${KUBECONFIG}"
        }

        dir("./kubernetes-deployment"){
            sh "kubectl delete -f ${deployment} || true"  
            sh "kubectl apply -f ${deployment} --validate=false"
        }
        // Get Kube IP
        getKubeIP("${credential}")
    }
}

def getKubeIP(credential){
    script {
        withCredentials([file(credentialsId: "${credential}", variable: 'config')]) { 
            sh "kubectl --kubeconfig=\$config get nodes -o wide | grep master | grep -o '[0-9]\\{1,3\\}\\.[0-9]\\{1,3\\}\\.[0-9]\\{1,3\\}\\.[0-9]\\{1,3\\}' > kube-ip.txt"
        }
        def ip = readFile 'kube-ip.txt'
        KUBE_IP = ip.toString()
        sh "rm -f kube-ip.txt"
	    echo "${KUBE_IP}"
    }
}

def dnsUpdate(host,zone,ip){
    echo "${host} ${zone} ${ip}"
    build job: "${DNS_PIPELINE}",  
          parameters: [ string(name: 'HOST_NAME', value: "${host}" ), 
                        string(name: 'HOST_ZONE', value: "${zone}" ), 
                        string(name: 'HOST_IP', value: "${ip}" )
                    ], wait: true
}

def robotTest(file,outputFolder){
    sh "mkdir -p robot-result/${outputFolder}" // Make Directory wait for result
    echo "${file}"
    echo "${outputFolder}"	    
    build job: "${ROBOT_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./robot-result/${outputFolder}"){
        sh 'ls'
    }
}

def jmeterTest(file,outputFolder){
    sh "mkdir -p jmeter-result/${outputFolder}" // Make Directory wait for result
    build job: "${JMETER_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./jmeter-result/${outputFolder}"){
        sh 'ls'
        // Publish JMeter Report
        perfReport "jmeterResultTest-${outputFolder}.csv"
    }
}